/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.watcharaphon.finalproject2;

import java.util.Random;

/**
 *
 * @author Lenovo
 */
public class CalScore {
    private int playerchoose;
    private int botchoose;

    public CalScore() {
    }

    public int getPlayerchoose() {
        return playerchoose;
    }

    public int getBotchoose() {
        return botchoose;
    }

    
    public int calResult(int playerchoose) { //rock=1 ,paper=2, scissors=3
        Random rn = new Random();
        this.playerchoose = playerchoose;         
        this.botchoose = rn.nextInt(3) + 1;
        if(this.playerchoose == this.botchoose) {
            return 0;
        }
        if(this.playerchoose == 1 && this.botchoose == 2) {
            return -1;
        }
        if(this.playerchoose == 1 && this.botchoose == 3) {
            return 1;
        }
        if(this.playerchoose == 2 && this.botchoose == 1) {
            return 1;
        }
        if(this.playerchoose == 2 && this.botchoose == 3) {
            return -1;
        }
        if(this.playerchoose == 3 && this.botchoose == 1) {
            return -1;
        }
        if(this.playerchoose == 3 && this.botchoose == 2) {
            return 1;
        }
        return 0;
    }
    
}
